/* Assignment 2: NGrams!
 * Pair programming by Alex Zhang and Pablo Martinez
 * Base functionality version
 *
 * This assignment asks the user to input a text file name, and a number n. This will
 * have the program read the text file, and saves a map of the first n-1 words as a key, and then
 * saves the nth word as the key's value. Later it will move one word to the right and save the new key and
 * word, until the text is saved on the map.
 *
 * Afterwards, the program will ask the user to set a number of words to generate randomly, and the program will select
 * a random key, and take a random number of the values saved, and will generate a set of new sentences from the saved map.
 *
 */

#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <iostream>
#include "filelib.h"
#include "simpio.h"
#include "strlib.h"
#include "console.h"
#include "map.h"
#include "vector.h"
#include "random.h"

using namespace std;

void printIntroMessage();
void getUserInput (ifstream & inputFile, int & NSize);
void buildNGram(Map<Vector<string>, Vector<string>>& NGramToBeFilled, int& NGramSize);
int getNumberOfWords(int NGramSize, int & numberOfWords);
void generateRandomWords(int NGramSize, Map<Vector<string>, Vector<string>> & NGramString);
void initializeKeyFrame(Map<Vector<string>, Vector<string>> & NGramString, Vector<string> & KeyFrame);
void initializeOutputNGramText(Vector<string> & KeyFrame, string & outputNGramText);
void generateRandomWord(Map<Vector<string>, Vector<string>> & NGramString, Vector<string> & KeyFrame, string & outputNGramText);
void slideKeyFrame(Vector<string> & KeyFrame, string & newWord);

int main() {
    Map<Vector<string>, Vector<string>> NGramString;
    int NSize = 0;

    printIntroMessage();
    buildNGram(NGramString,NSize);
    generateRandomWords(NSize,NGramString);
    cout << "Exiting." << endl;
    return 0;
}

/* void printIntroMessage()
* Input parameters: None
*
* Output parameters: None
*
* Description Here: Prints out welcome message and describes functionality
*
*/
void printIntroMessage(){
    cout << "Welcome to CS 106B/X Random Writer ('N-Grams')!"<< endl
         << "This program generates random text based on a document." << endl
         << "Give me an input file and an 'N' value for groups" << endl
         << "of words, and I'll create random text for you."<< endl << endl;
}

/* void buildNGram(Map<Vector<string>, Vector<string>> & NGramToBeFilled, int & NGramSize)
* Input parameters:
*
* Output parameters: NGramToBeFilled: Map to be filled with the input stream words
* NGramSize: N value prompted from the user
*
* Description Here: This function calls the user to input the text file to be read and the n
* size to be filled. Later it will read word by word, and it will save it into the Map, that will
* be used for the ramdom word generator.
*
*/
void buildNGram(Map<Vector<string>, Vector<string>> & NGramToBeFilled, int & NGramSize){
    ifstream inputFile;
    int vectorSize;
    string word;
    Vector<string> NGramKey;
    getUserInput(inputFile,NGramSize);
    //Read element by element and store into map
    while (inputFile >> word){
        //Check if key size is n-1 elements
        vectorSize=NGramKey.size();
        if (vectorSize < NGramSize-1){
            //No, continue building key
            NGramKey.add(word);
        }
        else {
            //Yes, save key and word into map
            NGramToBeFilled[NGramKey].add(word);
            //Call function to push new word into key
            slideKeyFrame(NGramKey,word);
        }
    }
    //Reset position of input stream, and redo to fill up the remaining files
    inputFile.clear();
    inputFile.seekg(0);
    //Do wrap read of the remaining n-1 elements
    for (int i = 0 ; i < NGramSize-1; i ++){
        inputFile >> word;
        //Save key and word into map
        NGramToBeFilled[NGramKey].add(word);
        //Call function to push into vector the value
        slideKeyFrame(NGramKey,word);
    }
}

/* void getUserInput (ifstream & fileIn, int & NSize)
* Input parameters:
*
* Output parameters: ifstream fileIn Input stream by the user
* int NSize: Value of N asked from the user
*
* Description Here
* This module asks the user for the file name and the ngram size, it will reject the user input,
* if he enters an invalid file name, or if N is less than 2
*
*/
void getUserInput (ifstream & fileIn, int & NSize){
    //Print out text file to be inputted
    promptUserForFile(fileIn, "Input file name? ");
    //Asks for user to input a valid Ngram size
    while (NSize < 2){
        NSize=getInteger("Value of N?");
        if (NSize < 2){
            cout << "N must be 2 or greater." << endl;
        }
    }
    cout << endl;
}



/*
 * Prompts user to input an integer number of words, and returns that value.
*/
/* int getNumberOfWords(int NGramSize, int & numberOfWords)
* Input parameters: Integer containing the current N-Gram size
*
* Output parameters: Integer containing how many words to be generated
*
* Description Here
* This function asks the user for the number of words to be generated.
* If the number provided is less than the size of the N-gram, then the user is asked
* to provide a new number.
*/
int getNumberOfWords(int NGramSize, int & numberOfWords) {
    numberOfWords = getInteger("# of random words to generate (0 to quit)?");

    while(numberOfWords < NGramSize && numberOfWords != 0) {
        cout << "Must be at least " + integerToString(NGramSize) + " words." << endl << endl;
        numberOfWords = getInteger("# of random words to generate (0 to quit)?");
    }
    return numberOfWords;
}

/* void initializeKeyFrame(Map<Vector<string>, Vector<string>> & NGramString, Vector<string> & KeyFrame)
* Input parameters: Map containing all valid NGram keys
*
* Output parameters: A randomized key from input map
*
* Description Here
* Picks a random key from a map and stores it into memory.  This acts as the starting point for our
* randomized word generator.
*/
void initializeKeyFrame(Map<Vector<string>, Vector<string>> & NGramString, Vector<string> & KeyFrame) {
    Vector<Vector<string>> KeyVector = NGramString.keys();
    int startingPosition = randomInteger(0, NGramString.size() - 1);
    KeyFrame = KeyVector[startingPosition];
}

/* void initializeOutputNGramText(Vector<string> & KeyFrame, string & outputNGramText)
* Input parameters: Vector containing the initial key used for word generaiton
*
* Output parameters: String containing the text to be printed onto the console
*
* Description Here
* This function simply adds "... " and the contents of the initial keyframe into the output
* variable.
*/
void initializeOutputNGramText(Vector<string> & KeyFrame, string & outputNGramText) {
    outputNGramText = "... ";

    // Add random start to output string
    for(int i = 0; i < KeyFrame.size(); i++) {
        outputNGramText += KeyFrame[i] + " ";
    }
}

/* void generateRandomWord(Map<Vector<string>, Vector<string>> & NGramString, Vector<string> & KeyFrame, string & nextWord)
* Input parameters: Map containing all valid NGram keys, vector containing one of the keys in map
*
* Output parameters: String containing the next word to be added to the output
*
* Description Here
* This function picks a random word from the map and key that are given as inputs.  It uses the key to look up the
* corresponding values in the map, which are stored in a vector of strings.  A randomly generated integer is used as an index
* to pull a random word from this vector, and this word is used to continue the random word generation process.
*/
void generateRandomWord(Map<Vector<string>, Vector<string>> & NGramString, Vector<string> & KeyFrame, string & nextWord) {
    Vector<string> NGramValue = NGramString[KeyFrame];
    int nextIndex = randomInteger(0, NGramValue.size() - 1);
    nextWord = NGramValue[nextIndex];
}

/* void generateRandomWords(int NGramSize, int numberOfWords, Map<Vector<string>, Vector<string>> & NGramString)
* Input parameters: Map with the generated key frames and corresponding values, size of the ngram to be filled
*
* Output parameters: None
*
* Description Here This is the second main method for the program, where the user is first asked how many words
* does he want to generate, and afterwards the method will choose a random key frame, save that keyframe to the
* output string, and later do a random selection of the values stored in that key for the next value out.  This
* function will be repeated, until the user inputs a 0 to finish the operation
*
*/
void generateRandomWords(int NGramSize, Map<Vector<string>, Vector<string>> & NGramString) {
    Vector<string> KeyFrame;
    string outputNGramText;
    string nextWord;
    int numberOfWords = 0;

    while(getNumberOfWords(NGramSize, numberOfWords)) {
        initializeKeyFrame(NGramString, KeyFrame);
        initializeOutputNGramText(KeyFrame, outputNGramText);

        for(int i = 0; i < numberOfWords - KeyFrame.size(); i++) {
            // Check to ensure new keyframe is valid and contained in map
            if(NGramString.containsKey(KeyFrame) == false) {
                cout << "Error: NGram map does not contain KeyFrame: " << KeyFrame << endl;
                break;
            }
            generateRandomWord(NGramString, KeyFrame, nextWord);
            slideKeyFrame(KeyFrame, nextWord);
            outputNGramText += nextWord + " ";
        }

        cout << outputNGramText << " ..." << endl << endl;
    }
}


/* void slideKeyFrame(Vector<string> & KeyFrame, string & newWord)
* Input parameters: Vector containing the current key and the last word that was randomly generated
*
* Output parameters: Vector containing the next key frame to be used for word generation
*
* Description Here
* This function removes the first word from the current key frame and appends the last generated word
* to the end of it.  This creates a new key frame that can be used to generate a new random word.
*
*/
void slideKeyFrame(Vector<string> & KeyFrame, string & newWord) {
    KeyFrame.removeFront();
    KeyFrame.add(newWord);
}
