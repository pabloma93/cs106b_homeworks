/*
 * CS 106B - Homework 3, Fractals
 * Alex Zhang, Pablo Martinez
 *
 * This project has two functions that interface with the Stanford GUI:
 * generating an n-order Sierpinski Triangle, and filling in a section of
 * solid color with another solid color.
 *
 * Sierpinski Triangle:
 * A Sierpinski Triangle is a fractal shape consisting of triangles nested
 * in one another.  This assignment is to construct an arbitrary Sierpinski
 * triangle, given the origin, size, and order, using recursive functions.
 * For more information, see here: https://en.wikipedia.org/wiki/Sierpi%C5%84ski_triangle
*/

#include "fractals.h"
using namespace std;

const double EQ_TRI_HEIGHT = 0.866;     // = sqrt(3)/2

// Function Prototypes
void drawSierpinskiLines(GWindow& window, double x, double y, double size);
void drawTriangle(GWindow& window, double x, double y, double size);
int doFloodFill (GWindow& window, int x, int y, int color, int baseColor);

/* Function: drawSierpinskiLines
 * Inputs: GUI window, origin (x, y), length of side, order
 * Outputs: A series of lines drawn on the GUI that correspond to these values
 *
 * Description:
 * The Sierpinski Triangle can be described as follows:
 * If n = 1, it is a simple triangle of side "size", with its upper left corner on the origin
 * Otherwise, it is a collection of 3 triangles of order (n - 1), whose origins are located
 * on the original origin, and halfway along the length of the two edges that intersect
 * said origin.
*/
void drawSierpinskiLines(GWindow& window, double x, double y, double size, int order) {
    if(order == 1) {
        drawTriangle(window, x, y, size);
    }

    else if(order == 0) {
        // Do nothing
    }

    else {
        drawSierpinskiTriangle(window, x, y, size/2, order - 1);
        drawSierpinskiTriangle(window, x + size/2, y, size/2, order - 1);
        drawSierpinskiTriangle(window, x + size/4, (y + size*EQ_TRI_HEIGHT/2), size/2, order - 1);
    }
}

/* Function: drawSierpinskiTriangle
 * Inputs: GUI window, origin (x, y), length of side, order
 * Outputs: A series of lines drawn on the GUI that correspond to these values
 *
 * Description:
 * This is a wrapper function that checks the inputs for errors (mainly negative inputs).
 * If there are no errors, drawSierpinskiLines contains the actual algorithm for generating
 * the Sierpinski Triangle.
*/

void drawSierpinskiTriangle(GWindow& window, double x, double y, double size, int order) {
    if(size < 0) {
        throw ("Error: Size must be 0 or greater.");
    }

    else if(x < 0) {
        throw ("Error: x must be 0 or greater.");
    }

    else if(y < 0) {
        throw ("Error: y must be 0 or greater.");
    }

    else if(order < 0) {
        throw ("Error: Order must be 0 or greater.");
    }

    // If no errors, run the Sierpinski Triangle function as normal
    else {
        drawSierpinskiLines(window, x, y, size, order);
    }
}

/* Function: drawTriangle
 * Inputs: GUI window, origin (x, y), length of side
 * Outputs: A series of lines drawn on the GUI that correspond to these values
 *
 * Description:
 * This function takes in an origin and a length, and draws an upside down triangle
 * with the upper left corner resting on the origin.
*/

void drawTriangle(GWindow& window, double x, double y, double size) {
    // Horizontal from top left to top right
    window.drawLine(x, y, x + size, y);
    // Diagonal from top right to bottom center
    window.drawLine(x + size, y, x + size/2, y + size*EQ_TRI_HEIGHT);
    // Diagonal from bottom center to top left
    window.drawLine(x + size/2, y + size*EQ_TRI_HEIGHT, x, y);
}

/* Function: floodFill
 * Inputs: GUI window, x and y coordinates, color to paint
 * Outputs: The number of pixels that were painted in the end
 *
 * Description:
 * This wrapper function is in charge of obtaining the color where the function starts, and passing down that parameter
 * into the recursive function itself. At the end, it returns the number of pixels that were modified using the recursive call
*/

int floodFill(GWindow& window, int x, int y, int color) {
    int baseColor = window.getPixel(x,y);
    int pix_count;
    pix_count = doFloodFill(window,x,y,color,baseColor);

    return pix_count;   // this is only here so it will compile
}

/* Function: doFloodFill
 * Inputs: GUI window, x and y coordinates, color to paint and base color taken from the first pixel
 * Outputs: The number of pixels that were painted each time the function ran
 *
 * Description:
 * This function is in charge of painting the pixel that is on the space when the function is called, only if
 * it is the same color as the first pixel recorded when calling out the floodFill method. After painting, it will
 * expand to the top, bottom left and right and redo the function, until it gets that the color it is trying to paint is not
 * the same one as the first color.
*/

int doFloodFill (GWindow& window, int x, int y, int color, int baseColor) {
    //Check for exception condition (x, y less than 0, or out of range)
    if (x < 0 || y < 0 || !window.inCanvasBounds(x,y)){
        throw ("Invalid coordinates, x or y must be greater than 0, or pixel is out of range");
    }
    //Check if pixel is in valid bounds
    if (window.inCanvasBounds(x,y)){
        //Obtain the color where the function is at the moment
        int currentColor = window.getPixel(x,y);
        int pixel_count = 0;
        //Check if the color is equal to the base color that the function started
        if (currentColor == baseColor){
            //Recursive case, paint the new color in this position
            window.setPixel(x,y,color);
            pixel_count ++;
            //Explore to the surroundings of the current pixel
            pixel_count += doFloodFill(window, x+1, y, color, baseColor);
            pixel_count += doFloodFill(window, x, y+1, color, baseColor);
            pixel_count += doFloodFill(window, x-1, y, color, baseColor);
            pixel_count += doFloodFill(window, x, y-1, color, baseColor);
            return pixel_count;
        }
    }
    //Base condition, return 0 pixels painted
    return 0;
}

