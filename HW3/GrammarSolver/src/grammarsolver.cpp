/*
 * CS 106B - Homework 3, Grammar Solver
 * Alex Zhang, Pablo Martinez
 *
 * This file will recieve an input stream from GrammarMain.cpp, and it will generate a Map where
 * it will store the values contained in the file. The characters are based upon the Backnus-Naur Form,
 * where symbols might be terminal or non terminal. A terminal is the simplest form that can be taken
 * by the grammar rule, as meanwhile a non terminal consists of a high level syntax of the language.
 *
 * The file must contain the structure as follows:
 * char1 ::= rule | rule | rule
 *
 * After each non--terminal block, a set of rules on how to transform the non*terminal block into a valid
 * sentence follows. The program will implement a mode where it will ask for a symbol, and generate a set
 * of expressions that match according to the inputted grammar.
 *
*/

#include "grammarsolver.h"
#include <string>
#include "map.h"
#include "simpio.h"
#include "strlib.h"

using namespace std;

// Private function prototypes
void mapGrammar(istream& input, Map<string, Vector<Vector<string>>>& Grammar);
string generateGrammarExpression (Map<string, Vector<Vector<string>>>& Grammar, string symbol);


/* grammarGenerate()
 * Inputs: input stream from file, symbol to check and number of times to generate
 *
 * Outputs: Vector of strings with the generated values
 *
 * Description:
 * This public method first uses the input stream to generate a Map containing the grammar rules,
 * and then later calls the function generateGrammarExpression to generate the different expressions
 * from the rule itself
*/

Vector<string> grammarGenerate(istream& input, string symbol, int times) {
    //Generate Grammar map first
    Vector<string> v;
    Map< string, Vector< Vector<string> > > grammar;
    mapGrammar(input, grammar);

    for(int i=0; i<times; i++){
        //Add Grammar expression based upon the symbol for n times
        v.add (generateGrammarExpression(grammar,symbol));
    }
    return v;
}

/*
 * Inputs: Input file containing grammar rules in BNF (Backus-Naur Form)
 *
 * Outputs: Map whose keys are non-terminals and values are rules
 *
 * Description:
 * The input file should be built from lines with the following format:
 * Non-Terminal ::= Rule | Rule | Rule
 *
 * Each Rule could be composed of multiple rules, separated by a single space
 *
 * This function decomposes each line into the non-terminal portion
 * and the rule portion.
 *
 * The non-terminals are strings that are set as the keys of the output map.
 *
 * The rules are each packaged into a Vector of strings, and the collection of
 * all rules is packaged into another vector.  This is set as the corresponding
 * value of the non-terminal.
*/

void mapGrammar(istream& input, Map<string, Vector<Vector<string>>>& Grammar) {
    string nextLine;

    while(getline(input, nextLine)) {
        // Split nextLine into 2 parts: non-terminal and rules
        size_t separator = nextLine.find("::=");

        string nonTerminal = nextLine.substr(0, separator);
        nonTerminal = trim(nonTerminal);

        //Check if there is a repeated nonTerminal block in the grammar, and throw an exception
        if (Grammar.containsKey(nonTerminal)){
            throw("Invalid grammar file, contains two (or more) lines for symbol " + nonTerminal);
        }
        string rules = nextLine.substr(separator + 3, string::npos);
        rules = trim(rules);

        // Convert rules string into vector
        Vector< Vector<string> > GrammarRules;

        Vector<string> SplitRules = stringSplit(rules, "|");
        for(int i = 0; i < SplitRules.size(); i++) {
            GrammarRules.add(stringSplit(SplitRules[i], " "));
        }

        // Add key and rules vector to the map
        Grammar.put(nonTerminal, GrammarRules);
    }
}

/* generateGrammar()
 * Inputs: Map containing the mapped grammar rules, symbol to generate Grammar rules
 *
 * Outputs: Random generated string based upon grammar rules
 *
 * Description:
 * After getting the map, this function generates sentences or expressions using the grammar rules in
 * the map. The function first checks if it is a non-terminal block or a terminal. If it is a terminal block
 * it returns the symbol back to the level above.
 *
 * If it is a non-terminal block, it obtains the rules related to that
 * non-terminal block, and selects a random rule. Afterwards it will build a sentence
 * according to the rule, by calling upon this function recursively.
*/

string generateGrammarExpression (Map<string, Vector<Vector<string>>>& Grammar, string symbol){
    string returnString;
    //Check for base case if it is a terminal symbol
    if (!Grammar.containsKey(symbol)){
        //Base case: The symbol is a terminal symbol, save it to the output string
        return symbol;
    }
    else {
        //Recursive case, obtain the different rules for non terminal symbol
        Vector <Vector <string>> Word=Grammar[symbol];
        //Generate a random number to obtain the rule
        int vectorNumber = randomInteger(0,Word.size()-1);
        //Loop around the chosen grammar rule
        for (int i=0;i<Word[vectorNumber].size();i++) {
            //Recursive case: The word is a non terminal word, call recursive function
            returnString += " " + generateGrammarExpression(Grammar,Word[vectorNumber][i]);
        }
    }
    //Trim output string
    returnString=trim(returnString);
    return returnString;
}
